<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;

class TodoController extends Controller
{
    public function todo(Request $request)
    {
        $allTodos = Todo::all();
        if ($request->type == "all") {
            $todos  = $allTodos;
        } else if ($request->type == "active") {
            $todos  = Todo::where('status', false)->get();
        } else if ($request->type == "completed") {
            $todos  = Todo::where('status', true)->get();
        }

        $leftItem = Todo::where('status', 0)->get()->count();
        $clearCompleted = $allTodos->count() - $leftItem == 0 ? false : true;

        $params = [
            'todos'     => isset($todos) ? $todos : false,
            'leftItem'  => $leftItem,
            'clearCompleted' => $clearCompleted,
            'haveData'      => $allTodos->count() != 0 ? true : false
        ];
        return $params;
    }

    public function delete(Request $request)
    {
        return json_encode(Todo::find($request->id)->delete());
    }

    public function clear()
    {
        $todos = Todo::where('status', true)->get();
        foreach ($todos as $todo) {
            if (!$todo->delete()) {
                return json_encode(false);
            }
        }
        return json_encode(true);
    }

    public function status(Request $request)
    {
        $todo = Todo::find($request->id);
        if ($todo->status == false) {
            $todo->status = true;
        } else {
            $todo->status = false;
        }
        return json_encode($todo->save());
    }

    public function allStatusChange(Request $request)
    {
        return json_encode(Todo::where('status', !$request->status)->update(['status' => $request->status]));
    }

    public function store(Request $request)
    {
        $is_modification = isset($request->id) ? true : false;
        $todo = $is_modification ? Todo::find($request->id) : new Todo;

        $todo->item = $request->item;

        if ($todo->save()) {
            return json_encode(true);
        }
        return json_encode(false);
    }
}
