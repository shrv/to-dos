{
    let type = "";

    function dataLoad(selected) {
        type = selected != "" ? selected : "all";
        $.ajax({
            type: "get",
            url: "/action/todo",

            data: {
                type: type
            }
        })
            .done(function(response) {
                let todos = response.todos;
                let leftItem = response.leftItem;
                let haveData = response.haveData;
                let clearCompleted = response.clearCompleted;

                allSelectMethod(leftItem, haveData);

                let data = dataListMethod(todos);

                let footer = footerMethod(haveData);

                document.getElementById("data").innerHTML = data + footer;

                if (haveData == true) {
                    leftItem += leftItem == 1 ? " item left" : " items left";

                    document.getElementById("leftItem").innerHTML = leftItem;

                    if (clearCompleted) {
                        document.getElementById("clearCompleted").innerHTML =
                            "Clear Completed";
                    } else {
                        document.getElementById("clearCompleted").innerHTML =
                            "";
                    }
                }
            })
            .fail(function(response) {
                console.log("dataLoad: failed");
            });
    }

    function statusChange(id) {
        if (id) {
            $.ajax({
                type: "post",
                url: "/action/todo/status",
                data: {
                    id: id
                }
            })
                .done(function(response) {
                    dataLoad(type);
                })
                .fail(function(response) {
                    console.log("statusChange: failed");
                });
        }
    }

    function allStatusChange() {
        let checked = document.getElementById("allChecked").checked ? 1 : 0;

        $.ajax({
            type: "post",
            url: "/action/todo/allstatuschange",
            data: {
                status: checked
            }
        })
            .done(function(response) {
                dataLoad(type);
            })
            .fail(function(response) {
                console.log("All StatusChange: failed");
            });
    }

    function clearCompleted() {
        if (confirm("All Completed will be Deleted... Sure to?")) {
            $.ajax({
                type: "post",
                url: "/action/todo/clear"
            })
                .done(function(response) {
                    dataLoad(type);
                })
                .fail(function(response) {
                    console.log("clearComplete: failed");
                });
        }
    }

    function dataStore() {
        document.onkeydown = function() {
            let item = document.getElementById("item");
            if (item != "") {
                if (event.keyCode === 13) {
                    $.ajax({
                        type: "post",
                        url: "/action/todo/store",

                        data: {
                            item: item.value
                        }
                    })
                        .done(function(response) {
                            dataLoad(type);
                        })
                        .fail(function(response) {
                            console.log("dataStore: failed");
                        });
                    item.value = "";
                }
            }
        };
    }

    function dataUpdate(id) {
        document.onkeydown = function() {
            let item = document.getElementById("item" + id);
            if (item != "") {
                if (event.keyCode === 13) {
                    $.ajax({
                        type: "post",
                        url: "/action/todo/store",

                        data: {
                            id: id,
                            item: item.value
                        }
                    })
                        .done(function(response) {
                            dataLoad(type);
                        })
                        .fail(function(response) {
                            console.log("dataStore: failed");
                        });
                    item.value = "";
                }
            }
        };
    }

    function dataListMethod(todos) {
        let data = "";
        for (let i = 0; i < todos.length; i++) {
            data +=
                "<div class='w-1/2 mx-auto flex shadow-lg border' id='data'>" +
                "<div class='w-1/12 flex justify-end bg-white cursor-default'>" +
                "<label class='flex justify-center items-center'>" +
                "<div class='bg-white border-2 rounded border-gray-400 w-6 h-6 flex flex-shrink-0 justify-center items-center mr-2 focus-within:border-blue-500'>" +
                "<input type='checkbox' class='opacity-0 absolute' " +
                (todos[i].status ? "checked" : "") +
                " onchange='statusChange(" +
                todos[i].id +
                ")'>" +
                "<svg class='fill-current hidden w-4 h-4 text-green-500 pointer-events-none' viewBox='0 0 20 20'>" +
                "<path d='M0 11l2-2 5 5L18 3l2 2L7 18z' />" +
                "</svg>" +
                "</div>" +
                "</label>" +
                "</div>" +
                "<div class='group w-11/12 flex justify-center items-center bg-white'>" +
                "<input type='text'" +
                "readonly='true'" +
                "ondblclick=\"this.readOnly='';\"" +
                "id='item" +
                todos[i].id +
                "' onkeyup='dataUpdate(" +
                todos[i].id +
                ")' class='rounded-rt-lg focus:outline-none w-full p-5 text-3xl cursor-default " +
                (todos[i].status ? "line-through text-gray-500" : "") +
                "' value='" +
                todos[i].item +
                "'>" +
                "<button onclick='deleteItem(" +
                todos[i].id +
                ")' class='hidden group-hover:block mr-5 px-2 border-pink-200 focus:outline-none focus:border-pink-200 rounded-md'><img class='h-5 w-5' src='/svg/close-24px.svg'/></button>" +
                "</div>" +
                "</div>";
        }
        return data;
    }

    function footerMethod(haveData) {
        let footer =
            haveData == true
                ? "<div class='w-1/2 mx-auto shadow-lg bg-white border'>" +
                  "<div class='flex justify-between'>" +
                  "<div class='w-3/14 text-center px-4 py-2 m-2' id='leftItem'></div>" +
                  "<div class='w-8/14 text-center px-4 py-2 m-2'>" +
                  "<button class='border px-2 py-1 hover:border-pink-200 focus:outline-none focus:border-pink-200 rounded-md " +
                  (type == "all" ? "border-pink-200" : "") +
                  "' onclick='allDataLoad()'>All</button>" +
                  "<button class='border px-2 py-1 mx-2 hover:border-pink-200 focus:outline-none focus:border-pink-200 rounded-md " +
                  (type == "active" ? "border-pink-200" : "") +
                  "' onclick='activeDataLoad()'>Active</button>" +
                  "<button class='border px-2 py-1 hover:border-pink-200 focus:outline-none focus:border-pink-200 rounded-md " +
                  (type == "completed" ? "border-pink-200" : "") +
                  "' onclick='completedDataLoad()'>Completed</button>" +
                  "</div>" +
                  "<div class='w-3/14 text-center px-4 py-2 m-2'>" +
                  "<button class='focus:outline-none hover:underline' onclick='clearCompleted()' id='clearCompleted'></button>" +
                  "</div>" +
                  "</div>" +
                  "</div>"
                : "";
        return footer;
    }

    function allSelectMethod(leftItem, haveData) {
        let allSelect =
            "<label class='flex justify-center items-center'>" +
            "<div class='bg-white border-2 rounded border-gray-400 w-6 h-6 flex flex-shrink-0 justify-center items-center mr-2 focus-within:border-blue-500'>" +
            "<input type='checkbox' class='opacity-0 absolute' " +
            (leftItem == 0 && haveData == true ? "checked" : "") +
            " title='All Completed' id='allChecked' onchange='allStatusChange()'>" +
            "<svg class='fill-current hidden w-4 h-4 text-green-500 pointer-events-none' viewBox='0 0 20 20'>" +
            "<path d='M0 11l2-2 5 5L18 3l2 2L7 18z' />" +
            "</svg>" +
            "</div>" +
            "</label>" +
            "<style>" +
            "input:checked+svg {" +
            "  display: block;" +
            "}" +
            "</style>";
        document.getElementById("allSelect").innerHTML = allSelect;
    }

    function deleteItem(id) {
        $.ajax({
            type: "post",
            url: "/action/todo/delete",
            data: {
                id: id
            }
        })
            .done(function(response) {
                dataLoad(type);
            })
            .fail(function(response) {
                console.log("Delete Item: failed");
            });
    }

    function allDataLoad() {
        dataLoad("all");
    }

    function activeDataLoad() {
        dataLoad("active");
    }

    function completedDataLoad() {
        dataLoad("completed");
    }
}
