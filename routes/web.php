<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/action/todo', 'TodoController@todo')->name('todo');
Route::post('/action/todo/status', 'TodoController@status')->name('status');
Route::post('/action/todo/allstatuschange', 'TodoController@allStatusChange')->name('allStatusChange');
Route::post('/action/todo/delete', 'TodoController@delete')->name('delete');
Route::post('/action/todo/clear', 'TodoController@clear')->name('clear');
Route::post('/action/todo/store', 'TodoController@store')->name('store');
