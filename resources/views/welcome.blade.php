<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>To-Dos</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="shortcut icon" href="/svg/sentiment_satisfied_alt-24px.svg">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>

<body class="bg-indigo-100" onload="dataLoad('all')">
    <div class="container mx-auto">
        <h1 class="font-normal text-6xl text-center text-teal-200" style="font-size:6em">
            to-dos
        </h1>
    </div>
    <div class="container mx-auto">
        <div class="w-1/2 mx-auto flex shadow-lg border">
            <div class="w-1/12 flex justify-end bg-white hover:cursor-pointer" id="allSelect">
                <!-- <label class="flex justify-center items-center">
                    <div class="bg-white border-2 rounded border-gray-400 w-6 h-6 flex flex-shrink-0 justify-center items-center mr-2 focus-within:border-blue-500">
                        <input type="checkbox" class="opacity-0 absolute" title="All Completed" id="allChecked" onchange="allStatusChange()">
                        <svg class="fill-current hidden w-4 h-4 text-green-500 pointer-events-none" viewBox="0 0 20 20">
                            <path d="M0 11l2-2 5 5L18 3l2 2L7 18z" />
                        </svg>
                    </div>
                </label>
                <style>
                    input:checked+svg {
                        display: block;
                    }
                </style> -->
            </div>
            <div class="w-11/12">
                <input type="text" class="rounded-rt-lg focus:outline-none w-full italic p-5 text-3xl" placeholder="My Bucket List ..." id="item" onkeypress="dataStore()">
            </div>
        </div>
        <div id="data"></div>
        <!-- <div class="w-1/2 mx-auto flex shadow-lg border" id="data">
            <div class="w-1/12 flex justify-end bg-white cursor-default" onclick="statusChange(5)">
                <label class="flex justify-center items-center">
                    <div class="bg-white border-2 rounded border-gray-400 w-6 h-6 flex flex-shrink-0 justify-center items-center mr-2 focus-within:border-blue-500">
                        <input type="checkbox" class="opacity-0 absolute">
                        <svg class="fill-current hidden w-4 h-4 text-green-500 pointer-events-none" viewBox="0 0 20 20">
                            <path d="M0 11l2-2 5 5L18 3l2 2L7 18z" />
                        </svg>
                    </div>
                </label>
                <style>
                    input:checked+svg {
                        display: block;
                    }
                </style>
            </div>
            <div class="group w-11/12 flex justify-center items-center bg-white">
                <input type="text" value="Sample" class="focus:outline-none w-full italic p-5 text-3xl cursor-default">
            </div>
        </div> -->
        <!-- <div class="w-1/2 mx-auto shadow-lg bg-white border">
            <div class="flex justify-between">
                <div class="text-center px-4 py-2 m-2" id="leftItem"></div>
                <div class="text-center px-4 py-2 m-2">
                    <button class="border px-2 hover:border-pink-200 focus:outline-none focus:border-pink-200 rounded-sm" onclick="dataLoad('all')">All</button>
                    <button class="border px-2 active:bg-pink-200 hover:border-pink-200 focus:outline-none focus:border-pink-200 rounded-sm" onclick="dataLoad('active')">Active</button>
                    <button class="border px-2 active:bg-pink-200 hover:border-pink-200 focus:outline-none focus:border-pink-200 rounded-sm" onclick="dataLoad('completed')">Completed</button>
                </div>
                <div class="text-center px-4 py-2 m-2">
                    <button class="focus:outline-none hover:underline" onclick="clearCompleted()" id="clearCompleted"></button>
                </div>
            </div>
        </div> -->
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/ajax.js') }}"></script>
</body>

</html>